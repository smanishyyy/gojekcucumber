$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/FeatureFile/Test2.feature");
formatter.feature({
  "line": 1,
  "name": "Credit card invalid",
  "description": "",
  "id": "credit-card-invalid",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user fills the card number as \"\u003cCardNumber\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user fills expiry date as \"\u003cExpiryDate\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user fills expiry cvv as \"\u003cCVV\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user enters the OTP as \"\u003cOTP\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "verify the fail message as \"\u003cFailMessage\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.examples({
  "line": 27,
  "name": "",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;",
  "rows": [
    {
      "cells": [
        "BrowserName",
        "CardNumber",
        "ExpiryDate",
        "CVV",
        "OTP",
        "ScreenhsotFoldername",
        "FailMessage"
      ],
      "line": 28,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;1"
    },
    {
      "cells": [
        "chrome",
        "4911 1111 11111113",
        "02/20",
        "123",
        "112233",
        "TC02_PaymentFail",
        "Your card got declined by the bank"
      ],
      "line": 29,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 967289,
  "status": "passed"
});
formatter.before({
  "duration": 199273,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "user launch the browser as \"chrome\" with demo url",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 28
    }
  ],
  "location": "TC01_PaySuccessfull.user_launch_the_browser_as_with_demo_url(String)"
});
formatter.result({
  "duration": 53769596784,
  "error_message": "org.openqa.selenium.TimeoutException: timeout\n  (Session info: chrome\u003d77.0.3865.120)\n  (Driver info: chromedriver\u003d2.39.562718 (9a2698cba08cf5a471a29d30c8b3e12becabb0e9),platform\u003dWindows NT 6.1.7601 SP1 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DELL-PC\u0027, ip: \u0027192.168.56.1\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_161\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.39.562718 (9a2698cba08cf5..., userDataDir: C:\\Users\\Dell\\AppData\\Local...}, cssSelectorsEnabled: true, databaseEnabled: false, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 77.0.3865.120, webStorageEnabled: true}\nSession ID: 8c53251560cc4a01ceb27ae4233959f9\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.get(RemoteWebDriver.java:277)\r\n\tat Base.BaseClass.initialization(BaseClass.java:44)\r\n\tat TestCase.TC01_PaySuccessfull.user_launch_the_browser_as_with_demo_url(TC01_PaySuccessfull.java:30)\r\n\tat ✽.Given user launch the browser as \"chrome\" with demo url(src/main/java/FeatureFile/Test2.feature:3)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 29,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user fills the card number as \"4911 1111 11111113\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user fills expiry date as \"02/20\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user fills expiry cvv as \"123\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user enters the OTP as \"112233\"",
  "matchedColumns": [
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "verify the fail message as \"Your card got declined by the bank\"",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_Buy_Now_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.verify_shipping_card_details_gets_populated_with_default_value()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_cliks_on_the_checkout_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_is_navigated_to_Order_Summary_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_continue_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.select_credit_card_as_payment_option()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "4911 1111 11111113",
      "offset": 31
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_the_card_number_as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "02/20",
      "offset": 27
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_date_as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 26
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_cvv_as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_the_paynow_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "112233",
      "offset": 24
    }
  ],
  "location": "TC01_PaySuccessfull.user_enters_the_OTP_as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_Ok_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TC02_PaymentFail.verify_that_payment_transaction_is_going_fail()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Your card got declined by the bank",
      "offset": 28
    }
  ],
  "location": "TC02_PaymentFail.verify_the_fail_message_as(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 9834603523,
  "status": "passed"
});
formatter.after({
  "duration": 223648,
  "status": "passed"
});
});