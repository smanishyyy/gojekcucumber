$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/FeatureFile/Test3.feature");
formatter.feature({
  "line": 1,
  "name": "Credit card invalid",
  "description": "",
  "id": "credit-card-invalid",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user fills the card number as \"\u003cCardNumber\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user fills expiry date as \"\u003cExpiryDate\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user fills expiry cvv as \"\u003cCVV\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user enters the OTP as \"\u003cOTP\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "verify the fail message as \"\u003cFailMessage\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.examples({
  "line": 27,
  "name": "",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;",
  "rows": [
    {
      "cells": [
        "BrowserName",
        "CardNumber",
        "ExpiryDate",
        "CVV",
        "OTP",
        "ScreenhsotFoldername",
        "FailMessage"
      ],
      "line": 28,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;1"
    },
    {
      "cells": [
        "chrome",
        "4911 1111 11111113",
        "02/20",
        "123",
        "112233",
        "TC02_PaymentFail",
        "Your card got declined by the bank"
      ],
      "line": 29,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 485783,
  "status": "passed"
});
formatter.before({
  "duration": 190293,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "user launch the browser as \"chrome\" with demo url",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 28
    }
  ],
  "location": "TC01_PaySuccessfull.user_launch_the_browser_as_with_demo_url(String)"
});
formatter.result({
  "duration": 34064906247,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user fills the card number as \"4911 1111 11111113\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user fills expiry date as \"02/20\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user fills expiry cvv as \"123\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user enters the OTP as \"112233\"",
  "matchedColumns": [
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "verify the fail message as \"Your card got declined by the bank\"",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 8817503216,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_Buy_Now_button()"
});
formatter.result({
  "duration": 1342545807,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.verify_shipping_card_details_gets_populated_with_default_value()"
});
formatter.result({
  "duration": 3260644448,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 746975406,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_cliks_on_the_checkout_button()"
});
formatter.result({
  "duration": 1059065742,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_is_navigated_to_Order_Summary_page()"
});
formatter.result({
  "duration": 31998007266,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_continue_button()"
});
formatter.result({
  "duration": 2564635988,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.select_credit_card_as_payment_option()"
});
formatter.result({
  "duration": 3358568498,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4911 1111 11111113",
      "offset": 31
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_the_card_number_as(String)"
});
formatter.result({
  "duration": 6579042684,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "02/20",
      "offset": 27
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_date_as(String)"
});
formatter.result({
  "duration": 2771794777,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 26
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_cvv_as(String)"
});
formatter.result({
  "duration": 1780298107,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_the_paynow_button()"
});
formatter.result({
  "duration": 2074695446,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "112233",
      "offset": 24
    }
  ],
  "location": "TC01_PaySuccessfull.user_enters_the_OTP_as(String)"
});
formatter.result({
  "duration": 22050130511,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1689553029,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_Ok_button()"
});
formatter.result({
  "duration": 9717054466,
  "status": "passed"
});
formatter.match({
  "location": "TC02_PaymentFail.verify_that_payment_transaction_is_going_fail()"
});
formatter.result({
  "duration": 162925,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Your card got declined by the bank",
      "offset": 28
    }
  ],
  "location": "TC02_PaymentFail.verify_the_fail_message_as(String)"
});
formatter.result({
  "duration": 32856018997,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1713838724,
  "status": "passed"
});
formatter.after({
  "duration": 31715481232,
  "status": "passed"
});
formatter.after({
  "duration": 139833,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 32,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 31,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 34,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user fills the card number as \"\u003cCardNumber\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user fills expiry date as \"\u003cExpiryDate\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "user fills expiry cvv as \"\u003cCVV\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "user enters the OTP as \"\u003cOTP\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "verify the fail message as \"\u003cFailMessage\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.examples({
  "line": 54,
  "name": "",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;",
  "rows": [
    {
      "cells": [
        "BrowserName",
        "CardNumber",
        "ExpiryDate",
        "CVV",
        "OTP",
        "ScreenhsotFoldername",
        "FailMessage"
      ],
      "line": 55,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;1"
    },
    {
      "cells": [
        "chrome",
        "4911 1111 11111113",
        "02/20",
        "123",
        "112233",
        "TC02_PaymentFail",
        "Your card got declined by the bank"
      ],
      "line": 56,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 215950,
  "status": "passed"
});
formatter.before({
  "duration": 141971,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "user launch the browser as \"chrome\" with demo url",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 28
    }
  ],
  "location": "TC01_PaySuccessfull.user_launch_the_browser_as_with_demo_url(String)"
});
formatter.result({
  "duration": 32062913086,
  "status": "passed"
});
formatter.scenario({
  "line": 56,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 31,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 34,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "user fills the card number as \"4911 1111 11111113\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "user fills expiry date as \"02/20\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "user fills expiry cvv as \"123\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "user enters the OTP as \"112233\"",
  "matchedColumns": [
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "verify the fail message as \"Your card got declined by the bank\"",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1071942822,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_Buy_Now_button()"
});
formatter.result({
  "duration": 1489367953,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.verify_shipping_card_details_gets_populated_with_default_value()"
});
formatter.result({
  "duration": 4418764027,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1020466146,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_cliks_on_the_checkout_button()"
});
formatter.result({
  "duration": 1324067264,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_is_navigated_to_Order_Summary_page()"
});
formatter.result({
  "duration": 31865776978,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_continue_button()"
});
formatter.result({
  "duration": 2191992203,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.select_credit_card_as_payment_option()"
});
formatter.result({
  "duration": 2072592811,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4911 1111 11111113",
      "offset": 31
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_the_card_number_as(String)"
});
formatter.result({
  "duration": 5965391432,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "02/20",
      "offset": 27
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_date_as(String)"
});
formatter.result({
  "duration": 2294587205,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 26
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_cvv_as(String)"
});
formatter.result({
  "duration": 1794108265,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_the_paynow_button()"
});
formatter.result({
  "duration": 1866403022,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "112233",
      "offset": 24
    }
  ],
  "location": "TC01_PaySuccessfull.user_enters_the_OTP_as(String)"
});
formatter.result({
  "duration": 18167469604,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1061845733,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_Ok_button()"
});
formatter.result({
  "duration": 3831951819,
  "status": "passed"
});
formatter.match({
  "location": "TC02_PaymentFail.verify_that_payment_transaction_is_going_fail()"
});
formatter.result({
  "duration": 304897,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Your card got declined by the bank",
      "offset": 28
    }
  ],
  "location": "TC02_PaymentFail.verify_the_fail_message_as(String)"
});
formatter.result({
  "duration": 32657103967,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1045743931,
  "status": "passed"
});
formatter.after({
  "duration": 13884862321,
  "status": "passed"
});
formatter.after({
  "duration": 123584,
  "status": "passed"
});
});