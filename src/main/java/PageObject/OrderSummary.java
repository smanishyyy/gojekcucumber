package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OrderSummary {
	public static WebDriver driver;
	public static String xpath;

	public OrderSummary(WebDriver driver2) {
		this.driver = driver2;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@class='content-table']//tr//span")
	public WebElement ItemName_EditBox;
	@FindBy(how = How.XPATH, using = "//*[@id='application']/div[1]/a")
	public WebElement Continue_Button;
	@FindBy(how = How.XPATH, using = "//div[@class='list-content']//div[contains(text(),'Credit Card')]")
	public WebElement PaymentOption_Button;

	public String getContinueButtonXapth() {
		xpath = "//*[contains(@class,'text-button-main')]//span";
		return xpath;

	}

}
