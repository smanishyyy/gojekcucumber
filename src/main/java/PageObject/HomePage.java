package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	public static WebDriver driver;

	public HomePage(WebDriver driver2) {
		this.driver = driver2;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'BUY NOW')]")
	public WebElement BuyNow_Button;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Thank you for')]")
	public WebElement Successfully_Message;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Your card')]")
	public WebElement Fail_Message;

}
