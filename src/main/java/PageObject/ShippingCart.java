package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ShippingCart {
	public static WebDriver driver;
	public static String xpath;

	public ShippingCart(WebDriver driver2) {
		this.driver = driver2;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//*[@class='table'])[2]//tbody//tr[1]//td//input")
	public WebElement Name_EditBox;
	@FindBy(how = How.XPATH, using = "(//*[@class='table'])[2]//tbody//tr[2]//td//input")
	public WebElement Email_EditBox;
	@FindBy(how = How.XPATH, using = "(//*[@class='table'])[2]//tbody//tr[3]//td//input")
	public WebElement PhoneNumber_EditBox;
	@FindBy(how = How.XPATH, using = "(//*[@class='table'])[2]//tbody//tr[4]//td//input")
	public WebElement City_EditBox;
	@FindBy(how = How.XPATH, using = "(//*[@class='table'])[2]//tbody//tr[5]//td[2]//textarea")
	public WebElement Address_EditBox;
	@FindBy(how = How.XPATH, using = "(//*[@class='table'])[2]//tbody//tr[6]//td//input")
	public WebElement PostalCode_EditBox;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'CHECKOUT')]")
	public WebElement CheckOut_Button;

	public String getNameXath() {

		xpath = "(//*[@class='table'])[2]//tbody//tr[1]//td//input";
		return xpath;

	}
}
