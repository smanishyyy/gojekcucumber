package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreditCard {
	public static WebDriver driver;
	public static String xpath;

	public CreditCard(WebDriver driver2) {
		this.driver = driver2;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//div[@class='card-container']//div//input)[1]")
	public WebElement CreditCardNumber_EditBox;
	@FindBy(how = How.XPATH, using = "(//div[@class='card-container']//div//input)[2]")
	public WebElement ExpiryDate_EditBox;
	@FindBy(how = How.XPATH, using = "(//div[@class='card-container']//div//input)[3]")
	public WebElement CVV_EditBox;
	@FindBy(how = How.XPATH, using = "//*[@id='application']/div[1]/a")
	public WebElement PayNow_Button;
	@FindBy(how = How.XPATH, using = "//*[@id='PaRes']")
	public WebElement Password_EditBox;
	@FindBy(how = How.XPATH, using = "//*[text()='OK']")
	public WebElement OK_Button;

	public String getPasswordXpath() {
		xpath = "//*[@id='PaRes']";
		return xpath;

	}

}
