package PageObjectManager;

import org.openqa.selenium.WebDriver;

import PageObject.CreditCard;
import PageObject.HomePage;
import PageObject.OrderSummary;
import PageObject.ShippingCart;

public class PageObjManager {
	public static WebDriver driver;
	public static HomePage HomePageobj;
	public static ShippingCart ShippingCartobj;
	public static OrderSummary OrderSummaryobj;
	public static CreditCard CreditCardobj;

	public PageObjManager(WebDriver driver) {
		this.driver = driver;
	}

	public static HomePage getHomePageobj() {
		HomePageobj = null;
		return (HomePageobj == null) ? HomePageobj = new HomePage(driver) : HomePageobj;

	}

	public static ShippingCart getShippingCartobj() {
		ShippingCartobj = null;
		return (ShippingCartobj == null) ? ShippingCartobj = new ShippingCart(driver) : ShippingCartobj;

	}

	public static OrderSummary getOrderSummaryobj() {
		OrderSummaryobj = null;
		return (OrderSummaryobj == null) ? OrderSummaryobj = new OrderSummary(driver) : OrderSummaryobj;

	}

	public static CreditCard getCreditCardobj() {
		CreditCardobj = null;
		return (CreditCardobj == null) ? CreditCardobj = new CreditCard(driver) : CreditCardobj;

	}
}
