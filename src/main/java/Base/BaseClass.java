
package Base;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass implements BaseInterface {

	public static WebDriver driver;

	@Override
	public void initialization(String browsername) {
		if (browsername.equals("chrome")) {

			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Drivers/chromedriver.exe");

			driver = new ChromeDriver();

		} else if (browsername.equals("firefox")) {
			// System.setProperty("webdriver.chrome.driver","C://Users//chaurma//eclipse-workspace//ServiceNowPageObjectModel//Drivers//chromedriver.exe");
			// driver=new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://demo.midtrans.com/");

	}

	@Override
	public File captureScreenMethod(WebDriver driver2, String TestcaseNumber) {
		File dst = null;
		try {
			SimpleDateFormat dft = new SimpleDateFormat("yyy_mm_dd_hh_mm_ss");

			String timeStamp = dft.format(new Date());
			TakesScreenshot tk = (TakesScreenshot) driver2;
			File src = tk.getScreenshotAs(OutputType.FILE);
			System.out.println(
					System.getProperty("user.dir") + "\\" + TestcaseNumber + "\\screenshots" + timeStamp + ".png");
			dst = new File(
					System.getProperty("user.dir") + "\\" + TestcaseNumber + "\\screenshots" + timeStamp + ".png");
			FileUtils.copyFile(src, dst);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dst;

	}

	@Override
	public WebElement waitVisibilityOfElementLocated(String string, int waitTime, WebDriver driver) {
		WebElement ele = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(string)));
			System.out.println("Inspected element : " + ele);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ele;
	}

	@Override
	public void ScrollToElementbyXpath(String Xpath, WebDriver driver2) {

		WebElement ele;

		try {
			ele = driver2.findElement(By.xpath(Xpath));
			JavascriptExecutor ex = (JavascriptExecutor) driver2;
			ex.executeScript("arguments[0].scrollIntoView(true);", ele);
			System.out.println("Scroll down : " + ele);
			Thread.sleep(3000);

		} catch (Exception e) {
			e.getMessage();

		}

	}

	@Override
	public WebElement waitVisibilityOf(WebElement element, int waitTime, WebDriver driver3) {

		WebElement ele = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			ele = wait.until(ExpectedConditions.visibilityOf(element));
			System.out.println("Inspected element : " + ele);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ele;

	}

	@Override
	public WebElement FluentWait(final String xapth, int waitTime, WebDriver driver3) {
		Wait<WebDriver> wt = new FluentWait<>(driver3).withTimeout(waitTime, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
				.ignoring(NullPointerException.class).ignoring(Exception.class);

		Function<WebDriver, WebElement> fn = new Function<WebDriver, WebElement>() {
			@Override
			public WebElement apply(WebDriver dr) {
				WebElement ele = dr.findElement(By.xpath(xapth));
				String val = ele.getAttribute("value");
				if (val.equalsIgnoreCase("WebDriver")) {
					return ele;
				} else {
					System.out.println("Defaul value : " + val);
					return null;
				}
			}

		};
		return wt.until(fn);

	}
}