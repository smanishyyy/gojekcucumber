package Base;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface BaseInterface {

	public File captureScreenMethod(WebDriver driver2, String TestcaseNumber);

	public void initialization(String browsername);

	public WebElement waitVisibilityOfElementLocated(String string, int waitTime, WebDriver driver);

	public void ScrollToElementbyXpath(String Xpath, WebDriver driver2);

	public WebElement waitVisibilityOf(WebElement element, int waitTime, WebDriver driver3);

	public WebElement FluentWait(String xpath, int waitTime, WebDriver driver3);
}
