Feature: Credit card payment feature4

Background: 
Given user launch the browser as "chrome" with demo url

@First
Scenario Outline: Varify credit card payment goes successfully with valid card number

And Take Screenshot "<ScreenhsotFoldername>" from first driver
When user clicks on the Buy Now button
Then verify shipping card details gets populated with default value
And Take Screenshot "<ScreenhsotFoldername>" from first driver
And user cliks on the checkout button
And user is navigated to Order Summary page
When user clicks on the continue button
And Take Screenshot "<ScreenhsotFoldername>" from first driver
And select credit card as payment option
And user fills the card number as "<CardNumber>"
And user fills expiry date as "<ExpiryDate>"
And user fills expiry cvv as "<CVV>"
And cliks on the paynow button
When user enters the OTP as "<OTP>"
And Take Screenshot "<ScreenhsotFoldername>" from first driver
And cliks on Ok button
Then verify that payment transaction is going successull
And Take Screenshot "<ScreenhsotFoldername>" from first driver
Then verify the successfull message as "<SuccessfullMessage>"
And Take Screenshot "<ScreenhsotFoldername>" from first driver


Examples:
|BrowserName|CardNumber|ExpiryDate|CVV|OTP|ScreenhsotFoldername|SuccessfullMessage|
|chrome|4811 1111 11111114|02/20|123|112233|TC01_PaySuccessfull|Thank you for your purchase.|

