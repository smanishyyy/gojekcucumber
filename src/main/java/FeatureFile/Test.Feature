Feature: Credit card invalid1
Background: 
Given user launch the browser as "chrome" with demo url
@Second
Scenario Outline: Varify credit card payment goes fail with invalid card number

And Take Screenshot "<ScreenhsotFoldername>" from first driver
When user clicks on the Buy Now button
Then verify shipping card details gets populated with default value
And Take Screenshot "<ScreenhsotFoldername>" from first driver
And user cliks on the checkout button
And user is navigated to Order Summary page
When user clicks on the continue button
And select credit card as payment option
And user fills the card number as "<CardNumber>"
And user fills expiry date as "<ExpiryDate>"
And user fills expiry cvv as "<CVV>"
And cliks on the paynow button
When user enters the OTP as "<OTP>"
And Take Screenshot "<ScreenhsotFoldername>" from first driver
And cliks on Ok button
Then verify that payment transaction is going fail
Then verify the fail message as "<FailMessage>"
And Take Screenshot "<ScreenhsotFoldername>" from first driver


Examples:
|BrowserName|CardNumber|ExpiryDate|CVV|OTP|ScreenhsotFoldername|FailMessage|
|chrome|4911 1111 11111113|02/20|123|112233|TC02_PaymentFail|Your card got declined by the bank|