
package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {

		"src/main/java/FeatureFile/TestPaymentScenario.feature"

}, plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/Extent Cucumber-reports/report.html" }, format = {

		"pretty", "html:target/cucumber-reports/cucumber-pretty",
		"json:target/cucumber-reports/CucumberTestReport.json", "junit:target/cucumber-reports/cucumber.xml",
		"rerun:target/cucumber-reports/rerun.txt" },

		glue = { "TestCase" },

		dryRun = false, monochrome = true, strict = true

// , tags = { "@First" }

)
public class testRunner1It {

	public static void writeExtentReport() {

	}
}
