package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {

		"src/main/java/FeatureFile/Test.feature"

}, plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target2/Extent Cucumber-reports/report.html" }, format = {

		"pretty", "html:target2/cucumber-reports/cucumber-pretty",
		"json:target2/cucumber-reports/CucumberTestReport.json", "junit:target/cucumber-reports/cucumber.xml",
		"rerun:target2/cucumber-reports/rerun.txt" },

		glue = { "TestCase" },

		dryRun = false, monochrome = true, strict = true

// , tags = {"@Second" }

)
public class testRunner2It {

}
