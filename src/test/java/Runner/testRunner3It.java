package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {

		"src/main/java/FeatureFile/Test2.feature"

}, plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target3/Extent Cucumber-reports/report.html" }, format = {

		"pretty", "html:target3/cucumber-reports/cucumber-pretty",
		"json:target3/cucumber-reports/CucumberTestReport.json", "junit:target/cucumber-reports/cucumber.xml",
		"rerun:target3/cucumber-reports/rerun.txt" },

		glue = { "TestCase" },

		dryRun = false, monochrome = true, strict = true

// , tags = {"@Second" }

)
public class testRunner3It {

}
