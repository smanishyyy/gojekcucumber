package TestCase;

import java.io.File;

import org.testng.Assert;

import com.cucumber.listener.Reporter;

import Base.BaseClass;
import PageObject.CreditCard;
import PageObject.HomePage;
import PageObject.OrderSummary;
import PageObject.ShippingCart;
import PageObjectManager.PageObjManager;
import cucumber.api.java.en.Then;

public class TC02_PaymentFail {
	public static PageObjManager PageObjManagerobj;
	public static HomePage HomePageobj;
	public static ShippingCart ShippingCartobj;
	public static OrderSummary OrderSummaryobj;
	public static CreditCard CreditCardobj;
	public static File fl;

	@Then("^verify that payment transaction is going fail$")
	public void verify_that_payment_transaction_is_going_fail() throws Throwable {

	}

	@Then("^verify the fail message as \"([^\"]*)\"$")
	public void verify_the_fail_message_as(String arg1) throws Throwable {
		PageObjManagerobj = new PageObjManager(BaseClass.driver);
		HomePageobj = PageObjManagerobj.getHomePageobj();
		BaseClass.driver.switchTo().defaultContent();
		BaseClass.driver.switchTo().frame("snap-midtrans");
		// BaseClass.driver.switchTo().frame(0);

		Assert.assertEquals(arg1, HomePageobj.Fail_Message.getText());
		Reporter.addStepLog("Message : " + HomePageobj.Fail_Message.getText());
	}

}
