package TestCase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import com.cucumber.listener.Reporter;

import Base.BaseClass;
import PageObject.CreditCard;
import PageObject.HomePage;
import PageObject.OrderSummary;
import PageObject.ShippingCart;
import PageObjectManager.PageObjManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TC01_PaySuccessfull extends BaseClass {
	public static PageObjManager PageObjManagerobj;
	public static HomePage HomePageobj;
	public static ShippingCart ShippingCartobj;
	public static OrderSummary OrderSummaryobj;
	public static CreditCard CreditCardobj;
	public static File fl;

	@Given("^user launch the browser as \"([^\"]*)\" with demo url$")
	public void user_launch_the_browser_as_with_demo_url(String arg1) {
		initialization(arg1);

	}

	@Given("^Take Screenshot \"([^\"]*)\" from first driver$")
	public void take_Screenshot_from_first_driver(String arg1) {
		fl = captureScreenMethod(BaseClass.driver, arg1);
		System.out.println("File: " + fl);

		try {
			Reporter.addScreenCaptureFromPath(fl.toString());
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@When("^user clicks on the Buy Now button$")
	public void user_clicks_on_the_Buy_Now_button() {
		PageObjManagerobj = new PageObjManager(BaseClass.driver);
		HomePageobj = PageObjManagerobj.getHomePageobj();
		HomePageobj.BuyNow_Button.click();

	}

	@Then("^verify shipping card details gets populated with default value$")
	public void verify_shipping_card_details_gets_populated_with_default_value() {
		ShippingCartobj = PageObjManagerobj.getShippingCartobj();
		waitVisibilityOfElementLocated(ShippingCartobj.getNameXath(), 30, BaseClass.driver);
		Reporter.addStepLog("Please find the customer details:");
		Reporter.addStepLog("Name : " + ShippingCartobj.Name_EditBox.getAttribute("value"));
		Reporter.addStepLog("Email : " + ShippingCartobj.Email_EditBox.getAttribute("value"));
		Reporter.addStepLog("City : " + ShippingCartobj.City_EditBox.getAttribute("value"));
		Reporter.addStepLog("PhoneNumber : " + ShippingCartobj.PhoneNumber_EditBox.getAttribute("value"));
		Reporter.addStepLog("Address : " + ShippingCartobj.Address_EditBox.getText());
		Reporter.addStepLog("PostalCode : " + ShippingCartobj.PostalCode_EditBox.getAttribute("value"));
	}

	@Then("^user cliks on the checkout button$")
	public void user_cliks_on_the_checkout_button() {
		ShippingCartobj.CheckOut_Button.click();
	}

	@Then("^user is navigated to Order Summary page$")
	public void user_is_navigated_to_Order_Summary_page() {
		BaseClass.driver.switchTo().frame("snap-midtrans");
		OrderSummaryobj = PageObjManagerobj.getOrderSummaryobj();
		Reporter.addStepLog("Item name : " + OrderSummaryobj.ItemName_EditBox.getText());

	}

	@When("^user clicks on the continue button$")
	public void user_clicks_on_the_continue_button() {

		OrderSummaryobj.Continue_Button.click();
	}

	@When("^select credit card as payment option$")
	public void select_credit_card_as_payment_option() throws Throwable {
		OrderSummaryobj.PaymentOption_Button.click();
	}

	@When("^user fills the card number as \"([^\"]*)\"$")
	public void user_fills_the_card_number_as(String arg1) throws Throwable {
		CreditCardobj = PageObjManagerobj.getCreditCardobj();
		CreditCardobj.CreditCardNumber_EditBox.sendKeys(arg1);
	}

	@When("^user fills expiry date as \"([^\"]*)\"$")
	public void user_fills_expiry_date_as(String arg1) throws Throwable {
		CreditCardobj.ExpiryDate_EditBox.sendKeys(arg1);
	}

	@When("^user fills expiry cvv as \"([^\"]*)\"$")
	public void user_fills_expiry_cvv_as(String arg1) throws Throwable {
		CreditCardobj.CVV_EditBox.sendKeys(arg1);
	}

	@When("^cliks on the paynow button$")
	public void cliks_on_the_paynow_button() throws Throwable {
		CreditCardobj.PayNow_Button.click();
	}

	@When("^user enters the OTP as \"([^\"]*)\"$")
	public void user_enters_the_OTP_as(String arg1) {

		try {
			BaseClass.driver.switchTo().frame(0);
			waitVisibilityOfElementLocated(CreditCardobj.getPasswordXpath(), 30, BaseClass.driver);
			CreditCardobj.Password_EditBox.click();
			CreditCardobj.Password_EditBox.sendKeys(arg1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@When("^cliks on Ok button$")
	public void cliks_on_Ok_button() throws Throwable {
		CreditCardobj.OK_Button.click();
	}

	@Then("^verify that payment transaction is going successull$")
	public void verify_that_payment_transaction_is_going_successull() throws Throwable {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Then("^verify the successfull message as \"([^\"]*)\"$")
	public void verify_the_successfull_message_as(String arg1) throws Throwable {
		waitVisibilityOf(HomePageobj.Successfully_Message, 30, BaseClass.driver);
		Assert.assertEquals(arg1, HomePageobj.Successfully_Message.getText());
		Reporter.addStepLog("Message : " + HomePageobj.Successfully_Message.getText());

	}

	@Then("^close the browser$")
	public void close_the_browser() throws Throwable {
		BaseClass.driver.quit();
	}

}
