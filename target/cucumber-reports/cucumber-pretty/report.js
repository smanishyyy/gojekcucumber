$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/FeatureFile/TestPaymentScenario.feature");
formatter.feature({
  "line": 1,
  "name": "Credit card payment feature",
  "description": "",
  "id": "credit-card-payment-feature",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 7,
  "name": "Varify credit card payment goes successfully with valid card number",
  "description": "",
  "id": "credit-card-payment-feature;varify-credit-card-payment-goes-successfully-with-valid-card-number",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 6,
      "name": "@First"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "user fills the card number as \"\u003cCardNumber\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user fills expiry date as \"\u003cExpiryDate\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user fills expiry cvv as \"\u003cCVV\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user enters the OTP as \"\u003cOTP\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "verify that payment transaction is going successull",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "verify the successfull message as \"\u003cSuccessfullMessage\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.examples({
  "line": 31,
  "name": "",
  "description": "",
  "id": "credit-card-payment-feature;varify-credit-card-payment-goes-successfully-with-valid-card-number;",
  "rows": [
    {
      "cells": [
        "BrowserName",
        "CardNumber",
        "ExpiryDate",
        "CVV",
        "OTP",
        "ScreenhsotFoldername",
        "SuccessfullMessage"
      ],
      "line": 32,
      "id": "credit-card-payment-feature;varify-credit-card-payment-goes-successfully-with-valid-card-number;;1"
    },
    {
      "cells": [
        "chrome",
        "4811 1111 11111114",
        "02/20",
        "123",
        "112233",
        "TC01_PaySuccessfull",
        "Thank you for your purchase."
      ],
      "line": 33,
      "id": "credit-card-payment-feature;varify-credit-card-payment-goes-successfully-with-valid-card-number;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 413514,
  "status": "passed"
});
formatter.before({
  "duration": 119307,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "user launch the browser as \"chrome\" with demo url",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 28
    }
  ],
  "location": "TC01_PaySuccessfull.user_launch_the_browser_as_with_demo_url(String)"
});
formatter.result({
  "duration": 46337580650,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Varify credit card payment goes successfully with valid card number",
  "description": "",
  "id": "credit-card-payment-feature;varify-credit-card-payment-goes-successfully-with-valid-card-number;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 6,
      "name": "@First"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "Take Screenshot \"TC01_PaySuccessfull\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "Take Screenshot \"TC01_PaySuccessfull\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "Take Screenshot \"TC01_PaySuccessfull\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "user fills the card number as \"4811 1111 11111114\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user fills expiry date as \"02/20\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user fills expiry cvv as \"123\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user enters the OTP as \"112233\"",
  "matchedColumns": [
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Take Screenshot \"TC01_PaySuccessfull\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "verify that payment transaction is going successull",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "Take Screenshot \"TC01_PaySuccessfull\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "verify the successfull message as \"Thank you for your purchase.\"",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Take Screenshot \"TC01_PaySuccessfull\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "TC01_PaySuccessfull",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1444265887,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_Buy_Now_button()"
});
formatter.result({
  "duration": 1076586832,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.verify_shipping_card_details_gets_populated_with_default_value()"
});
formatter.result({
  "duration": 2630523636,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC01_PaySuccessfull",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 2979225964,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_cliks_on_the_checkout_button()"
});
formatter.result({
  "duration": 1511621145,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_is_navigated_to_Order_Summary_page()"
});
formatter.result({
  "duration": 32328027502,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_continue_button()"
});
formatter.result({
  "duration": 2218103431,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC01_PaySuccessfull",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 2653008172,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.select_credit_card_as_payment_option()"
});
formatter.result({
  "duration": 2057746517,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4811 1111 11111114",
      "offset": 31
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_the_card_number_as(String)"
});
formatter.result({
  "duration": 6625968999,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "02/20",
      "offset": 27
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_date_as(String)"
});
formatter.result({
  "duration": 1994936917,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 26
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_cvv_as(String)"
});
formatter.result({
  "duration": 1553036225,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_the_paynow_button()"
});
formatter.result({
  "duration": 1394365011,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "112233",
      "offset": 24
    }
  ],
  "location": "TC01_PaySuccessfull.user_enters_the_OTP_as(String)"
});
formatter.result({
  "duration": 23230561866,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC01_PaySuccessfull",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1422857683,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_Ok_button()"
});
formatter.result({
  "duration": 4644387675,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.verify_that_payment_transaction_is_going_successull()"
});
formatter.result({
  "duration": 44045,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC01_PaySuccessfull",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1365917667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you for your purchase.",
      "offset": 35
    }
  ],
  "location": "TC01_PaySuccessfull.verify_the_successfull_message_as(String)"
});
formatter.result({
  "duration": 11007284169,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC01_PaySuccessfull",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 1369168901,
  "status": "passed"
});
formatter.after({
  "duration": 43935817722,
  "status": "passed"
});
formatter.after({
  "duration": 62006,
  "status": "passed"
});
});