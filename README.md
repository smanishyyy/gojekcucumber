# GoJekAssignment : Hybrid framework using BDD framework

High level details to run this assignment project.

TestRunner configuration with test cases:
"testRunner" class in "Runner" package is engine to run this project.
It contains feature file path (TestPaymentScenario.java in "FeatureFile" package) where we have scenario that has to be validated 
And steps definition file's path means test cases's package path ("TestCase") and glues both of them.

Reporting format:
Using extent report.

Steps to run the "testRunner.java"

Create a workspace in your system and keep that project in workspace.
Then import this project in IDE (Eclipse) and one more thing cucumber plugin should be installed in eclipse
After importing it will takes some time to download all the required dependency jar from POM.xml.
Then navigate to the "testRunner.java" class.
Right click on the "testRunner.java" class.
And select run as "Junit test" and project will be running.
After running your report "report.html" would be generated in the "target\Extent Cucumber-reports" folder

Or

1) Right click on the project
2) And run as "Maven test"




