$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/FeatureFile/Test.feature");
formatter.feature({
  "line": 1,
  "name": "Credit card invalid",
  "description": "",
  "id": "credit-card-invalid",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user fills the card number as \"\u003cCardNumber\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user fills expiry date as \"\u003cExpiryDate\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user fills expiry cvv as \"\u003cCVV\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user enters the OTP as \"\u003cOTP\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "verify the fail message as \"\u003cFailMessage\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Take Screenshot \"\u003cScreenhsotFoldername\u003e\" from first driver",
  "keyword": "And "
});
formatter.examples({
  "line": 27,
  "name": "",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;",
  "rows": [
    {
      "cells": [
        "BrowserName",
        "CardNumber",
        "ExpiryDate",
        "CVV",
        "OTP",
        "ScreenhsotFoldername",
        "FailMessage"
      ],
      "line": 28,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;1"
    },
    {
      "cells": [
        "chrome",
        "4911 1111 11111113",
        "02/20",
        "123",
        "112233",
        "TC02_PaymentFail",
        "Your card got declined by the bank"
      ],
      "line": 29,
      "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 929230,
  "status": "passed"
});
formatter.before({
  "duration": 118025,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "user launch the browser as \"chrome\" with demo url",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 28
    }
  ],
  "location": "TC01_PaySuccessfull.user_launch_the_browser_as_with_demo_url(String)"
});
formatter.result({
  "duration": 61600335002,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Varify credit card payment goes fail with invalid card number",
  "description": "",
  "id": "credit-card-invalid;varify-credit-card-payment-goes-fail-with-invalid-card-number;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Second"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on the Buy Now button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify shipping card details gets populated with default value",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user cliks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user is navigated to Order Summary page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user clicks on the continue button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "select credit card as payment option",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user fills the card number as \"4911 1111 11111113\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user fills expiry date as \"02/20\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user fills expiry cvv as \"123\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "cliks on the paynow button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user enters the OTP as \"112233\"",
  "matchedColumns": [
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "cliks on Ok button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "verify that payment transaction is going fail",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "verify the fail message as \"Your card got declined by the bank\"",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Take Screenshot \"TC02_PaymentFail\" from first driver",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 2200102545,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_Buy_Now_button()"
});
formatter.result({
  "duration": 1052081338,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.verify_shipping_card_details_gets_populated_with_default_value()"
});
formatter.result({
  "duration": 2972759414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 5902860215,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_cliks_on_the_checkout_button()"
});
formatter.result({
  "duration": 1170798667,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_is_navigated_to_Order_Summary_page()"
});
formatter.result({
  "duration": 31445282780,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.user_clicks_on_the_continue_button()"
});
formatter.result({
  "duration": 1215940930,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.select_credit_card_as_payment_option()"
});
formatter.result({
  "duration": 1458097854,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4911 1111 11111113",
      "offset": 31
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_the_card_number_as(String)"
});
formatter.result({
  "duration": 3375031644,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "02/20",
      "offset": 27
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_date_as(String)"
});
formatter.result({
  "duration": 1731424813,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 26
    }
  ],
  "location": "TC01_PaySuccessfull.user_fills_expiry_cvv_as(String)"
});
formatter.result({
  "duration": 1781979529,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_the_paynow_button()"
});
formatter.result({
  "duration": 1276939110,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "112233",
      "offset": 24
    }
  ],
  "location": "TC01_PaySuccessfull.user_enters_the_OTP_as(String)"
});
formatter.result({
  "duration": 14384610023,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 803947922,
  "status": "passed"
});
formatter.match({
  "location": "TC01_PaySuccessfull.cliks_on_Ok_button()"
});
formatter.result({
  "duration": 2980112004,
  "status": "passed"
});
formatter.match({
  "location": "TC02_PaymentFail.verify_that_payment_transaction_is_going_fail()"
});
formatter.result({
  "duration": 181741,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Your card got declined by the bank",
      "offset": 28
    }
  ],
  "location": "TC02_PaymentFail.verify_the_fail_message_as(String)"
});
formatter.result({
  "duration": 32290691967,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC02_PaymentFail",
      "offset": 17
    }
  ],
  "location": "TC01_PaySuccessfull.take_Screenshot_from_first_driver(String)"
});
formatter.result({
  "duration": 770523020,
  "status": "passed"
});
formatter.after({
  "duration": 27051764468,
  "status": "passed"
});
formatter.after({
  "duration": 54736,
  "status": "passed"
});
});